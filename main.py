# -*- coding: utf-8 -*-

## -------- main.py ---------

from Tkinter import *
# from othello import Othello
from othello2 import Othello
from tkMessageBox import *
import random, time, sys

def grid(g):
    for i in range(8):
        for j in range(8):
            pos = (i * 50, j * 50, (i + 1) * 50, (j + 1) * 50)
            g.append(c0.create_rectangle(pos, fill = 'green4', tags = "grid"))

def result():
    b1.configure(state = ACTIVE)
    b2.configure(state = ACTIVE)

    bc = 0; wc = 0
    for i in range(8):
        for j in range(8):
            p = i + 1 + (j + 1) * 10
            if otl.state[p] == Othello.BLACK:
                bc += 1
            elif otl.state[p] == Othello.WHITE:
                wc += 1

    restxt = u"黒：" + str(bc) + u"   白：" + str(wc)
    if bc > wc:
        showinfo(title = 'result', message = restxt + u"\n 黒の勝ち")
    elif bc < wc:
        showinfo(title = 'result', message = restxt + u"\n 白の勝ち")
    else:        
        showinfo(title = 'result', message = restxt + u"\n 引き分け")
    
def randp():
    if otl.finish: 
        result(); return
    plc = otl.put_place()
#     p = random.choice(plc)
    p = otl.eval_func(plc , int(w.get()))
    otl.put_stone(p)
    set_state()

    # repeat
    root.after(10, randp)
        
def random_play():
    restart()
    b1.configure(state = DISABLED)
    b2.configure(state = DISABLED)
    randp()


def wait_computer():
    otl.computer_select(int(w.get()))
    set_state()
    if otl.finish: result()
    elif otl.color != my_color:
#         root.after(1000, wait_computer)
        if opts.get(): root.after(1000, wait_computer)
        else: root.after(10, wait_computer)

def select(event):
    x, y, d1, d2 = c0.coords("current")
#     print x, y, x/50, y/50, int(x), int(y)
# なんか知らんけど +1 しないとおかしい場合がある
    x, y = int((x + 1) / 50), int((y + 1) / 50)
    p = x + 1 + (y + 1) * 10
#     print x, y, p, otl.put_place()
    if player == 0:
        if otl.put_stone(p):
            set_state()
            if otl.finish: result()
    else:
        if my_color == otl.color and otl.put_stone(p):
            set_state()
            if otl.finish: 
                result(); return
#             root.after(1000, wait_computer)
            if opts.get(): root.after(1000, wait_computer)
            else: root.after(10, wait_computer)
            
        
        

def change(event):
    if otl.color != my_color and my_color != None: return
    x, y, d1, d2 = c0.coords("current")
    x, y = int((x + 1) / 50), int((y + 1) / 50)
    p = x + 1 + (y + 1) * 10
    if p in otl.put_place():
        c0.itemconfigure("current", fill = "green")

def change2(event):
    x, y, d1, d2 = c0.coords("current")
    x, y = int((x + 1) / 50), int((y + 1) / 50)
    p = x + 1 + (y + 1) * 10
    if p in otl.put_place():
        c0.itemconfigure("current", fill = "lightgreen")
    else:
        c0.itemconfigure("current", fill = "green4")
        
def init():
    global bc_text, wc_text, line
    c1.delete(bc_text)
    c1.delete(wc_text)
    c1.delete(line)
    bc_text = c1.create_text(110, 115, anchor = "e", text = 2, \
                                 font = ('FixedSys', 14))
    wc_text = c1.create_text(110, 215, anchor = "e", text = 2, \
                                 font = ('FixedSys', 14))
    line = c1.create_line((50, 140, 80, 140), fill = 'red', width = 2.0)

def init_board():
    for i, n in enumerate(stone):
        if n != None:
            c0.delete(n)
            stone[i] = None


# 状態の表示
def set_state():
    bc = 0
    wc = 0
    plc = otl.put_place()
    for i in range(8):
        for j in range(8):
            p = i + 1 + (j + 1) * 10
            if otl.state[p] == Othello.BLACK:
                bc += 1
                if stone[p] == None:
                    coord = (i * 50 + 5, j * 50 + 5, \
                             (i + 1) * 50 - 5, (j + 1) * 50 - 5)
                    stone[p] = c0.create_oval(coord, fill = "black")
                else:
                    c0.itemconfigure(stone[p], fill = "black")
            if otl.state[p] == Othello.WHITE:
                wc += 1
                if stone[p] == None:
                    coord = (i * 50 + 5, j * 50 + 5, \
                             (i + 1) * 50 - 5, (j + 1) * 50 - 5)
                    stone[p] = c0.create_oval(coord, fill = "white")
                else:
                    c0.itemconfigure(stone[p], fill = "white")
            if p in plc:
                c0.itemconfigure(grids[j + i * 8], fill = "lightgreen")
            else:
                c0.itemconfigure(grids[j + i * 8], fill = "green4")

    c1.itemconfigure(bc_text, text = bc)
    c1.itemconfigure(wc_text, text = wc)
    if otl.color == Othello.BLACK:
        c1.coords(line, (50, 140, 80, 140))
    else: # otl.color == Othello.white:
        c1.coords(line, (50, 240, 80, 240))

def restart():
    otl.init()
    init()
    init_board()
    set_state()
    global player, my_color
    player = 0
    my_color = None
    b1.configure(state = NORMAL)
    b2.configure(state = NORMAL)

def npc_black():
    global my_color, player
    if my_color != None: return 
    restart()
    b1.configure(state = DISABLED)
    b2.configure(state = DISABLED)
    player = 1
    my_color = Othello.WHITE
    otl.computer_select()
    set_state()

def npc_white():
    global my_color, player
    if my_color != None: return 
    restart()
    b1.configure(state = DISABLED)
    b2.configure(state = DISABLED)
    player = 1
    my_color = Othello.BLACK

    



###################################################################
otl = Othello()

root = Tk()
root.title("othello")
root.minsize(610, 405)
# root.maxsize(600, 400)


c0 = Canvas(root, width = 400, height = 400)

la = Label(root, text='depth')
#la.grid(row = 1, column = 0, padx = 5, pady = 5)
w = Spinbox(root, from_ = 1, to = 4, increment = 1, width = 3,
                    state = 'readonly')

#w.grid(row = 1, column = 1, padx = 5, pady = 5)

opts = BooleanVar()
opts.set(True)
cb = Checkbutton(text = 'wait', variable = opts)

c1 = Canvas(root, width = 200, height = 350, bg = "gray")


# c0.pack(side = 'left')
# la.pack(side = "top")
# w.pack()
# cb.pack()
# c1.pack(side = "left")

c0.grid(row = 0, column = 0, rowspan = 3)
la.grid(row = 0, column = 1)
w.grid(row = 1, column = 1)
cb.grid(row = 0, column = 2, rowspan = 2)
c1.grid(row = 2, column = 1, columnspan = 2)


menubar = Menu(root)
root.configure(menu = menubar)

# メニューの設定
games = Menu(menubar, tearoff = False)
npc_color = Menu(menubar, tearoff = False)
menubar.add_cascade(label="Games", underline = 0, menu=games)
games.add_command(label = "reset", under = 0, command = restart)
games.add_cascade(label = 'VS computer', under = 0, menu = npc_color)
games.add_command(label = "random", under = 0, command = random_play)
games.add_command(label = "exit", under = 0, command = sys.exit)

npc_color.add_command(label = "first", under = 0, command = npc_white)
npc_color.add_command(label = "second", under = 0, command = npc_black)


c1.create_oval((50, 100, 80, 130), fill = "black")
c1.create_oval((50, 200, 80, 230), fill = "white")

b1 = Button(root, text = 'Black', state = NORMAL, command = npc_white)
b2 = Button(root, text = 'White', state = NORMAL, command = npc_black)

c1.create_window(150, 100, window = b1, anchor = "nw", width = 50)
c1.create_window(150, 200, window = b2, anchor = "nw", width = 50)



grids = []
grid(grids)
stone = [None] * 100
bc_text = None
wc_text = None
line = None
player = 0
my_color = None
restart()

        
c0.tag_bind("grid", "<ButtonPress>", select)
c0.tag_bind("grid", "<Enter>", change)
c0.tag_bind("grid", "<Leave>", change2)

root.mainloop()
