# -*- coding: utf-8 -*-

## -------- othello2.py --------

import random, sys

class Othello:
    BLACK = 1
    WHITE = -1
    NONE = 0
    WALL = 99
    DOWN = 10
    UP = -DOWN
    RIGHT = 1
    LEFT = -1
    UL = UP + LEFT
    UR = UP + RIGHT
    DL = DOWN + LEFT
    DR = DOWN + RIGHT
    AROUND = (UL, LEFT, DL, DOWN, DR, RIGHT, UR, UP)
    corner = (11, 18, 81, 88)
    star = (22, 27, 72, 77)
    def __init__(self):
        self.state = []
        self.color = self.BLACK
        self.finish = False
        self.init()

    def init(self):
        self.color = self.BLACK
        self.finish = False
        self.state = []
        for i in range(10): self.state.append(self.WALL)
        for i in range(3):
             self.state.append(self.WALL)
             for j in range(8): self.state.append(self.NONE)
             self.state.append(self.WALL)
        for i in (self.WHITE, self.BLACK):
             self.state.append(self.WALL)
             for j in range(3): self.state.append(self.NONE)
             self.state.append(i); self.state.append(-i); 
             for j in range(3): self.state.append(self.NONE)
             self.state.append(self.WALL)
        for i in range(3):
             self.state.append(self.WALL)
             for j in range(8): self.state.append(self.NONE)
             self.state.append(self.WALL)
        for i in range(10): self.state.append(self.WALL)

    def print_state(self):
        for i, x in enumerate(self.state):
            if x == self.NONE: print "-",
            elif x == self.BLACK: print "x",
            elif x == self.WHITE: print "o",
            if i % 10 == 9: print

    def subtract(self, color):
        ret = 0
        for i in range(1, 10):
            for j in range(1, 10):
                p = i + j * 10
                if self.state[p] == color:
                    ret += 1
                elif self.state[p] == -color:
                    ret -= 1
        return ret
                

    # n の位置に石を置いたとき、方向 d で返せるか
    def direction(self, color, n, d):
        if self.state[n] == color:
            return True
        elif self.state[n] == self.NONE or self.state[n] == self.WALL:
            return False
        else:
            return self.direction(color, n + d, d)
            
    # 置ける場所のリストを返す
    def put_place(self, color = None):
        ret = []
        if color == None: color = self.color
        for i, x in enumerate(self.state):
            if self.put(color, i): ret.append(i)
        return ret

    # n の位置に置けるか
    def put(self, color, n):
        if self.state[n] != self.NONE: return False
        for d in self.AROUND:
            if self.state[n + d] == -color:
                if self.direction(color, n + d, d): 
                    return True
        else:
            return False
        

    # 方向 d について返せるならひっくり返す
    def reverse_stone(self, color, n, d):
        if self.state[n] == color:
            return True
        elif self.state[n] == self.NONE or self.state[n] == self.WALL:
            return False
        else:
            if self.reverse_stone(color, n + d, d):
                self.state[n] = color
                return True
            else:
                return False


    # n の位置に置いてひっくり返す。＋手番の判定とか
    def put_stone(self, n):
        if self.put(self.color, n):
            self.state[n] = self.color
            for d in self.AROUND:
                self.reverse_stone(self.color, n + d, d)

            self.color = -self.color # 相手の手番になる

            if len(self.put_place()) > 0: return True # 相手が置けたらそのまま
            else:                            # 置けない場合、
                self.color = -self.color     # 自分の番に戻ってくる
                if len(self.put_place()) > 0: return True
                self.finish = True # 相手も自分も置けないなら
                return True
        else:
            return False
                
    # ひっくり返すだけ
    def _put_stone(self, n, color):
        self.state[n] = color
        for d in self.AROUND:
            self.reverse_stone(color, n + d, d)


# ------------------------------ eval function ------------------------------
    def eval_star(self, color):
        point = 0
        for i, n in enumerate(self.star):
            if self.state[n] == color:
                if self.state[self.corner[i]] == self.NONE:
                    point -= 5
            elif self.state[n] == -color:
                if self.state[self.corner[i]] == self.NONE:
                    point += 5
        return point

    def eval_corner(self, color):
        point = 0
        for i in self.corner:
            if self.state[i] == -color:
                point -= 10
            elif self.state[i] == color: 
                point += 10
        return point

    def include_corner(self, pos):
        ret = 0
        for p in pos:
            if p in self.corner:
                ret += 1
        return ret

    # 石のおける数の差と隅に置いてある石の数の差
    def eval_f(self, color):
        t = self.put_place(-self.color)
        myp = len(self.put_place(self.color))
        otp = len(t)
        if myp == otp == 0:
            sub = self.subtract(self.color)
            if sub > 0:
                return 100
            elif sub < 0:
                return -100
            else: return 0
        else:
            point = myp - otp
            point += self.eval_star(self.color) + self.eval_corner(self.color) \
                - self.include_corner(t) * 10
            # print point
            return point

    def minimax(self, d, color):
        if d <= 1:
            return self.eval_f(-color)
        else:
            copy = self.state[:]
            p = self.put_place(color)
            if len(p) == 0: return self.minimax(d - 1, color)

            lst = []
            for i in p:
                self._put_stone(i, color)
                lst.append(self.minimax(d - 1, -color))
                self.state = copy[:]

            if color == self.color: 
                # print "eval =", max(lst), "depth =", d, "max"
                return max(lst)
            else: 
                # print "eval =", min(lst), "depth =", d, "min"
                return min(lst)

    def alpha_beta(self, d, color, alpha = -sys.maxint, beta = sys.maxint):
        if d <= 1:
            return self.eval_f(-color)
        else:
            copy = self.state[:]
            p = self.put_place(color)
            if len(p) == 0: return self.alpha_beta(d - 1, color, alpha, beta)

            if color == self.color: 
                for i in p:
                    self._put_stone(i, color)
                    alpha = max(alpha, self.alpha_beta(d - 1, -color, alpha, beta))
                    self.state = copy[:]
                    if alpha >= beta: return beta
                return alpha
            else:
                for i in p:
                    self._put_stone(i, color)
                    beta = min(beta, self.alpha_beta(d - 1, -color, alpha, beta))
                    self.state = copy[:]
                    if alpha >= beta: return alpha
                return beta


    def eval_func(self, p, d = 1):
        num = -sys.maxint

        copy = self.state[:]
        lst = []
        for i in p:
            self._put_stone(i, self.color)
            # n = self.minimax(d, -self.color) # 第一引数：深さ
            n = self.alpha_beta(d, -self.color) # 第一引数：深さ
            self.state = copy[:]

            if num < n: 
                num = n
                lst = [i]
            elif num == n:
                lst.append(i)

        # print "eval =", num, "result"
        return random.choice(lst)

    def computer_select(self, d = 1):
        plc = self.put_place()
        if len(plc) == 0: print plc
        p = self.eval_func(plc, d)
        self.put_stone(p)


    def start(self):
        self.init()
        self.print_state()

        while not self.finish:
            plc = self.put_place()
#             p = None
#             while p not in plc:
#                 p = int(raw_input(str(plc) + ": "))
            # p = random.choice(plc)
            p = self.eval_func(plc)
            
            self.put_stone(p)
            self.print_state()
            raw_input()

if __name__ == "__main__":
    # import time
    # t = time.time(); print t
    # random.seed(t)
    otl = Othello()
    otl.start()
#     otl.print_state()
#     print otl.put_place()
